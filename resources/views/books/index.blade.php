
@extends('layouts.app')
@section('content')
<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

<div class='container'>
    <br/><br/>
    <h2>My book list :</h2>
      <table class="table table-bordered">
          <thead class="thead-dark">
            <tr>
              <th scope="col">Id</th>
              <th scope="col"> Book title</th>
              <th scope="col">Author name</th>
              <th scope="col">Areated at</th>
              <th scope="col">Updated at</th>
              <th scope="col">Status</th>
            </tr>
          </thead>
      
          <tbody>
          @foreach($books as $book)
            <tr>
                <td>@can('manager') <a href = "{{route('books.edit' , $book->id)}}"> {{$book->id}} </a>@endcan 
                   @cannot('manager')
                                {{$book->id}}
                    @endcannot
                </td>
                <td> @can('manager') <a href = "{{route('books.edit' , $book->id)}}"> {{$book->title}} </a>@endcan 
                     @cannot('manager')
                         {{$book->title}}
                      @endcannot
                </td>
                <td>{{$book->author}}</td>
                <td>{{$book->created_at}}</td> 
                <td>{{$book->updated_at}}</td>
                <td>  @if ($book->status)
                        <input type = 'checkbox' id ="{{$book->id}}"  disabled='disable' checked>
                  @else
                        <input type = 'checkbox' id ="{{$book->id}}">
                  @endif
                </td>  
            </tr>
            @endforeach

          </tbody>
    </table>
    <div class ="container">
        <div class="col-3  offset-4">
       
        @can('manager')
        <a href="{{route('books.create')}}" class=" btn btn-secondary">Add another books to your list</a>
        @endcan
       
       
        <script>
       $(document).ready(function(){
           $(":checkbox").click(function(event){
            console.log(event.target.id)

           $(this).attr('disabled', true);
            alert("Thanks for you read my book");
               $.ajax({
                   url:"{{url('books')}}" + '/' + event.target.id,
                   dataType: 'json',
                   type:'put',
                   contentType: 'application/json',
                   data:  JSON.stringify({'status':event.target.checked, _token:'{{csrf_token()}}'}),
                   processData: false,
                   success: function( data){
                   },
                   error: function(errorThrown ){
                   }
               });               
           });
       });
   </script>  
        </div>
    </div>
</div>
@endsection
