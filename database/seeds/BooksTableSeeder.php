<?php

use Illuminate\Database\Seeder;

class BooksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('books')->insert(
        [
            [
                'id'=>1 ,
                'title'=> 'Harry Potter and the Deathly Hallows' ,
                'author'=>'J.K. Rowling' ,
                'user_id'=>1 ,
                'created_at'=>date('Y-m-d G:i:s') ,
                'updated_at'=>date('Y-m-d G:i:s') ,
            ],
            [
                'id'=>2 ,
                'title'=> 'The Name of the Wind' ,
                'author'=> 'by Patrick Rothfuss',
                'user_id'=>1 ,
                'created_at'=>date('Y-m-d G:i:s') ,
                'updated_at'=>date('Y-m-d G:i:s') ,
            ],
            [
                'id'=>3 , 
                'title'=> 'Clockwork Princess' ,
                'author'=> 'by Cassandra Clare ',
                'user_id'=>1 ,
                'created_at'=>date('Y-m-d G:i:s') ,
                'updated_at'=>date('Y-m-d G:i:s') ,
            ],
            [
                'id'=>4 ,
                'title'=> 'The Way of Kings' ,
                'author'=> 'by Brandon Sanderson' ,
                'user_id'=>1 , 
                'created_at'=>date('Y-m-d G:i:s') ,
                'updated_at'=>date('Y-m-d G:i:s') ,
            ],
            [
                'id'=>5 ,
                'title'=> 'The Wise Mans Fear' ,
                'author'=> 'by Patrick Rothfuss' ,
                'user_id'=>1 ,
                'created_at'=>date('Y-m-d G:i:s') ,
                'updated_at'=>date('Y-m-d G:i:s') ,
            ],
            [
                'id'=>6 ,
                'title'=> 'This is Going to Hurt' ,
                'author'=>'Adam Kay' ,
                'user_id'=>2 ,
                'created_at'=>date('Y-m-d G:i:s') ,
                'updated_at'=>date('Y-m-d G:i:s') ,
            ],
            [
                'id'=>7 ,
                'title'=> 'Dear Zoo' ,
                'author'=> 'Rod Campbell',
                'user_id'=>2 ,
                'created_at'=>date('Y-m-d G:i:s') ,
                'updated_at'=>date('Y-m-d G:i:s') ,
            ],
            [
                'id'=>8 , 
                'title'=> 'The Tattooist of Auschwitz' ,
                'author'=> 'Heather Morris ',
                'user_id'=>2 ,
                'created_at'=>date('Y-m-d G:i:s') ,
                'updated_at'=>date('Y-m-d G:i:s') ,
            ],
            [
                'id'=>9 ,
                'title'=> 'The Very Hungry Caterpillar' ,
                'author'=> 'Eric Carle' ,
                'user_id'=>2 , 
                'created_at'=>date('Y-m-d G:i:s') ,
                'updated_at'=>date('Y-m-d G:i:s') ,
            ],
            [
                'id'=>10 ,
                'title'=> 'Look Inside Your Body' ,
                'author'=> 'Louie Stowell' ,
                'user_id'=>2 ,
                'created_at'=>date('Y-m-d G:i:s') ,
                'updated_at'=>date('Y-m-d G:i:s') ,
            ]
        ]
   );
    }
}


