<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            [
               [
               'name' => 'Eliran',
               'email' => 'eliransr@gmail.com',
               'password' =>Hash::make('12345678'),
               'created_at' => date('Y-m-d G:i:s')
               ],
               [
                'name' => 'Matan',
                'email' => 'matan@gmail.com',
                'password' =>Hash::make('12345678'),
                'created_at' => date('Y-m-d G:i:s')
               ],
               [
                'name'=>'gilad',
                'email'=>'gilad@gilad.com',
                'password'=>Hash::make('12345678'),
                'created_at' => date('Y-m-d G:i:s'),
                
               ],
               [
                'name'=>'gad',
                'email'=>'gad@gad.com',
                'password'=>Hash::make('12345678'),
                'created_at' => date('Y-m-d G:i:s'),
                
               ],
               [
                'name'=>'moshe',
                'email'=>'moshe@moshe.com',
                'password'=>Hash::make('12345678'),
                'created_at' => date('Y-m-d G:i:s'),
                
               ],
               [
                'name'=>'kobi',
                'email'=>'kobi@moshe.com',
                'password'=>Hash::make('12345678'),
                'created_at' => date('Y-m-d G:i:s'),
                
               ]
               
            
           ]);
    }
}

