<?php

use Illuminate\Database\Seeder;

class EmployeesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('employees')->insert([
            [
                'employee' => 3,
                "manager" => 1
            ],
            [
                'employee' => 4,
                "manager" => 1
            ],
            [
                'employee' =>5,
                "manager" => 2
            ],
            [
                'employee' =>6,
                "manager" => 2
            ]
        ]);
    }
}
